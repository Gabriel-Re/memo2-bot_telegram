require_relative '../app/proveedor_viaje'

describe 'ProveedorViaje' do
  # Si importo con el env me rompen los demas tests
  let(:api_key) { 'eGRdT5Ied4zbWqxiva0ejUoc10jg2Z0RZ7MZeedBNK0' }

  xit 'invocacion al proveedor de viaje' do
    WebMock.disable!
    proveedor_viaje = ProveedorViaje.new(api_key, nil)
    expect(proveedor_viaje.calcular_viaje('-34.6060,-58.4570', '-34.6094484,-58.4558905')).to be >= 0
    WebMock.enable!
  end
end
