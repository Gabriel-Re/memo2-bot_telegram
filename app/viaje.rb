class Viaje
  def initialize(proveedor_viaje)
    @proveedor_viaje = proveedor_viaje
  end

  def calcular_viaje_en_minutos(latitud1, longitud1, latitud2, longitud2)
    # Creo el origen y el destino
    origin = "#{latitud1}%2C#{longitud1}"
    destination = "#{latitud2}%2C#{longitud2}"

    @proveedor_viaje.calcular_viaje(origin, destination) / 60
  end
end
