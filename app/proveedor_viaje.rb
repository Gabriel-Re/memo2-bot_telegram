class ProveedorViaje
  def initialize(api_key, base_url)
    @api_key = api_key || ENV['HERE_API_KEY']
    @base_url = base_url || 'https://router.hereapi.com'
  end

  def calcular_viaje(origin, destination)
    connection = Faraday::Connection.new @base_url
    routes_url = "/v8/routes?transportMode=car&origin=#{origin}&destination=#{destination}&return=summary&apikey=#{@api_key}"

    response = connection.get routes_url

    response_json = JSON.parse(response.body)
    if response.success? && !response_json['routes'].empty?
      response_json['routes'][0]['sections'][0]['summary']['duration']
    else
      raise ErrorViaje
    end
  end
end
