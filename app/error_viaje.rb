class ErrorViaje < StandardError
  def initialize
    super('Error al calcular el viaje')
  end
end
