require "#{File.dirname(__FILE__)}/../lib/routing"
require "#{File.dirname(__FILE__)}/../lib/version"
require "#{File.dirname(__FILE__)}/tv/series"
require "#{File.dirname(__FILE__)}/proveedor_viaje"
require "#{File.dirname(__FILE__)}/viaje"
require "#{File.dirname(__FILE__)}/error_viaje"

class Routes
  include Routing

  on_message '/start' do |bot, message|
    bot.api.send_message(chat_id: message.chat.id, text: "Hola, #{message.from.first_name}")
  end

  on_message_pattern %r{/say_hi (?<name>.*)} do |bot, message, args|
    bot.api.send_message(chat_id: message.chat.id, text: "Hola, #{args['name']}")
  end

  on_message '/stop' do |bot, message|
    bot.api.send_message(chat_id: message.chat.id, text: "Chau, #{message.from.username}")
  end

  on_message '/time' do |bot, message|
    bot.api.send_message(chat_id: message.chat.id, text: "La hora es, #{Time.now}")
  end

  on_message '/tv' do |bot, message|
    kb = [Tv::Series.all.map do |tv_serie|
      Telegram::Bot::Types::InlineKeyboardButton.new(text: tv_serie.name, callback_data: tv_serie.id.to_s)
    end]
    markup = Telegram::Bot::Types::InlineKeyboardMarkup.new(inline_keyboard: kb)

    bot.api.send_message(chat_id: message.chat.id, text: 'Quien se queda con el trono?', reply_markup: markup)
  end

  on_message '/busqueda_centro' do |bot, message|
    kb = [
      Telegram::Bot::Types::KeyboardButton.new(text: 'Compartime tu ubicacion', request_location: true)
    ]
    markup = Telegram::Bot::Types::ReplyKeyboardMarkup.new(keyboard: kb)
    bot.api.send_message(chat_id: message.chat.id, text: 'Busqueda por ubicacion', reply_markup: markup)
  end

  on_message_pattern %r{/viaje (?<latitud1>.*), (?<longitud1>.*), (?<latitud2>.*), (?<longitud2>.*)} do |bot, message, args|
    latitud1 = args['latitud1']
    longitud1 = args['longitud1']
    latitud2 = args['latitud2']
    longitud2 = args['longitud2']

    if latitud1.nil? || longitud1.nil? || latitud2.nil? || longitud2.nil?
      bot.api.send_message(chat_id: message.chat.id, text: 'Ingresa la latitud y longitud de ambos puntos')
    else
      begin
        proveedor_viaje = ProveedorViaje.new(ENV['HERE_API_KEY'], 'https://router.hereapi.com')
        viaje = Viaje.new(proveedor_viaje)
        tiempo_calculado = viaje.calcular_viaje_en_minutos(latitud1, longitud1, latitud2, longitud2)

        bot.api.send_message(chat_id: message.chat.id, text: "Vas a tardar #{tiempo_calculado} minutos en hacer tu viaje en auto")
      rescue ErrorViaje
        bot.api.send_message(chat_id: message.chat.id, text: 'Ocurrió un error al calcular el viaje')
      end
    end
  end

  on_location_response do |bot, message|
    response = "Ubicacion es Lat:#{message.location.latitude} - Long:#{message.location.longitude}"
    puts response
    bot.api.send_message(chat_id: message.chat.id, text: response)
  end

  on_response_to 'Quien se queda con el trono?' do |bot, message|
    response = Tv::Series.handle_response message.data
    bot.api.send_message(chat_id: message.message.chat.id, text: response)
  end

  on_message '/version' do |bot, message|
    bot.api.send_message(chat_id: message.chat.id, text: Version.current)
  end

  default do |bot, message|
    bot.api.send_message(chat_id: message.chat.id, text: 'Uh? No te entiendo! Me repetis la pregunta?')
  end
end
